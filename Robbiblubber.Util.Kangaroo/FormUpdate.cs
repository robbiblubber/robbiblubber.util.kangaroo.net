﻿using System;
using System.Runtime.Versioning;
using System.Windows.Forms;



namespace Robbiblubber.Util.Kangaroo
{
    /// <summary>This class implements the update window.</summary>
    internal partial class FormUpdate: Form, IWindow
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        public FormUpdate()
        {
            InitializeComponent();

            _TimeTimer.Enabled = true;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Timer tick.</summary>
        private void _TimeTimer_Tick(object sender, EventArgs e)
        {
            _TimeTimer.Enabled = false;
            Program._RunSetup();
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IWindow                                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Sets the window message.</summary>
        /// <param name="text">Message text.</param>
        /// <param name="progress">Progress.</param>
        public void SetProgress(string text, int progress)
        {
            if(text != null) { _LabelMessage.Text = text; }
            if(progress <= _PbarProgress.Maximum) { _PbarProgress.Value = progress; }

            Application.DoEvents();
        }


        /// <summary>Sets the window message.</summary>
        /// <param name="text">Message text.</param>
        public void SetProgress(string text)
        {
            SetProgress(text, -1);
        }


        /// <summary>Sets the window message.</summary>
        /// <param name="progress">Progress.</param>
        public void SetProgress(int progress)
        {
            SetProgress(null, progress);
        }


        /// <summary>Signals the UI window to complete the installation process.</summary>
        /// <param name="success">Determines if the installation has succeeded.</param>
        public void Complete(bool success)
        {
            Close();
        }
    }
}
