﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Robbiblubber.Util.Kangaroo
{
    /// <summary>User interface windows implement this interface.</summary>
    internal interface IWindow
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Sets the window message.</summary>
        /// <param name="text">Message text.</param>
        /// <param name="progress">Progress.</param>
        void SetProgress(string text, int progress);


        /// <summary>Sets the window message.</summary>
        /// <param name="text">Message text.</param>
        void SetProgress(string text);


        /// <summary>Sets the window message.</summary>
        /// <param name="progress">Progress.</param>
        void SetProgress(int progress);



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Signals the UI window to complete the installation process.</summary>
        /// <param name="success">Determines if the installation has succeeded.</param>
        void Complete(bool success);
    }
}
