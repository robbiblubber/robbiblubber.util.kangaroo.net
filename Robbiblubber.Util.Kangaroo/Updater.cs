﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;

using Robbiblubber.Util;



namespace Robbiblubber.Util.Kangaroo
{
    /// <summary>This class allows access to Kangaroo updater functionality.</summary>
    public static class Updater
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns if an update is available for this application.</summary>
        /// <param name="updatePath">Update path.</param>
        /// <returns>Returns TRUE if an update is available, otherwise returns FALSE.</returns>
        public static bool UpdateAvailavle(string updatePath)
        {
            updatePath = updatePath.TrimEnd(' ', '/', '\\');
            Ddp avail;
            if(updatePath.StartsWith("http://") || updatePath.StartsWith("https://") || updatePath.StartsWith("ftp://"))
            {
                avail = Ddp.Open(updatePath + "/install.config");
            }
            else
            {
                avail = Ddp.Open(updatePath + @"\install.config");
            }

            Ddp current;
            if(File.Exists(PathOp.LocalConfigurationPath + @"\robbiblubber.org\instll\" + avail.Get("key", "") + ".vsig"))
            {
                current = Ddp.Open(PathOp.LocalConfigurationPath + @"\robbiblubber.org\instll\" + avail.Get("key", "") + ".vsig");
            }
            else
            {
                current = new Ddp();

                current.Set("key", avail.Get("key", ""));
                current.Set("version", "0.0.0");
                current.Set("install", PathOp.ApplicationDirectory);
                current.Set("source", updatePath);

                current.Save(PathOp.LocalConfigurationPath + @"\robbiblubber.org\instll\" + avail.Get("key", "") + ".vsig");
            }
            

            return ((new Version(avail.Get("version", "0.0.0.0"))) > (new Version(current.Get("version", "0.0.0.0"))));
        }


        /// <summary>Starts an update.</summary>
        /// <param name="updatePath">Update path.</param>
        public static void Update(string updatePath)
        {
            updatePath = updatePath.TrimEnd(' ', '/', '\\');
            string temp = PathOp.TempRoot + @"\" + StringOp.Unique();
            string tempFile = temp + ".bag";

            WebClient wc = new WebClient();
            wc.DownloadFile("http://robbiblubber.org/instll/kangaroo/kangaroo.bag", tempFile);
            wc.Dispose();

            Directory.CreateDirectory(temp);
            ZipFile.ExtractToDirectory(tempFile, temp);

            Ddp ddp = new Ddp();

            ddp.Set("source", updatePath);
            ddp.Set("install", PathOp.ApplicationDirectory);
            ddp.Set("start", Environment.CommandLine.Trim());
            ddp.Save(temp + @"\install.data");

            try { File.Delete(tempFile); } catch(Exception) {}

            try
            {
                Process p = new Process();
                p.StartInfo = new ProcessStartInfo(temp + @"\Kangaroo.exe");
                p.StartInfo.UseShellExecute = true;
                p.Start();
            }
            catch(Exception) {}
        }
    }
}
