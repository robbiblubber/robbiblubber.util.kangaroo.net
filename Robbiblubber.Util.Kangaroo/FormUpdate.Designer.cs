﻿namespace Robbiblubber.Util.Kangaroo
{
    partial class FormUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUpdate));
            this._LabelRobbiblubber = new System.Windows.Forms.Label();
            this._PanelWindow = new System.Windows.Forms.Panel();
            this._LabelMessage = new System.Windows.Forms.Label();
            this._LabelKangaroo = new System.Windows.Forms.Label();
            this._TimeTimer = new System.Windows.Forms.Timer(this.components);
            this._PbarProgress = new Robbiblubber.Util.Controls.ProgressControl();
            this._PanelWindow.SuspendLayout();
            this.SuspendLayout();
            // 
            // _LabelRobbiblubber
            // 
            this._LabelRobbiblubber.Image = ((System.Drawing.Image)(resources.GetObject("_LabelRobbiblubber.Image")));
            this._LabelRobbiblubber.Location = new System.Drawing.Point(441, 0);
            this._LabelRobbiblubber.Name = "_LabelRobbiblubber";
            this._LabelRobbiblubber.Size = new System.Drawing.Size(123, 29);
            this._LabelRobbiblubber.TabIndex = 0;
            // 
            // _PanelWindow
            // 
            this._PanelWindow.BackColor = System.Drawing.Color.WhiteSmoke;
            this._PanelWindow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._PanelWindow.Controls.Add(this._PbarProgress);
            this._PanelWindow.Controls.Add(this._LabelMessage);
            this._PanelWindow.Controls.Add(this._LabelKangaroo);
            this._PanelWindow.Controls.Add(this._LabelRobbiblubber);
            this._PanelWindow.ForeColor = System.Drawing.Color.DarkGray;
            this._PanelWindow.Location = new System.Drawing.Point(0, 0);
            this._PanelWindow.Name = "_PanelWindow";
            this._PanelWindow.Size = new System.Drawing.Size(560, 80);
            this._PanelWindow.TabIndex = 0;
            // 
            // _LabelMessage
            // 
            this._LabelMessage.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelMessage.ForeColor = System.Drawing.Color.Gray;
            this._LabelMessage.Location = new System.Drawing.Point(109, 48);
            this._LabelMessage.Name = "_LabelMessage";
            this._LabelMessage.Size = new System.Drawing.Size(446, 13);
            this._LabelMessage.TabIndex = 0;
            this._LabelMessage.Text = "Kangaroo installer is looking for updates...";
            // 
            // _LabelKangaroo
            // 
            this._LabelKangaroo.Image = ((System.Drawing.Image)(resources.GetObject("_LabelKangaroo.Image")));
            this._LabelKangaroo.Location = new System.Drawing.Point(6, -6);
            this._LabelKangaroo.Name = "_LabelKangaroo";
            this._LabelKangaroo.Size = new System.Drawing.Size(78, 79);
            this._LabelKangaroo.TabIndex = 0;
            // 
            // _TimeTimer
            // 
            this._TimeTimer.Interval = 420;
            this._TimeTimer.Tick += new System.EventHandler(this._TimeTimer_Tick);
            // 
            // _PbarProgress
            // 
            this._PbarProgress.BarColor = System.Drawing.Color.CornflowerBlue;
            this._PbarProgress.Location = new System.Drawing.Point(-1, 65);
            this._PbarProgress.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._PbarProgress.Maximum = 100;
            this._PbarProgress.Name = "_PbarProgress";
            this._PbarProgress.Size = new System.Drawing.Size(560, 14);
            this._PbarProgress.TabIndex = 1;
            this._PbarProgress.Value = 0;
            // 
            // FormUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(560, 80);
            this.Controls.Add(this._PanelWindow);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormUpdate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kangaroo";
            this._PanelWindow.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label _LabelRobbiblubber;
        private System.Windows.Forms.Panel _PanelWindow;
        private System.Windows.Forms.Label _LabelKangaroo;
        private System.Windows.Forms.Timer _TimeTimer;
        private System.Windows.Forms.Label _LabelMessage;
        private Util.Controls.ProgressControl _PbarProgress;
    }
}

