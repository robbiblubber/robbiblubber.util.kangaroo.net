﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;



namespace Robbiblubber.Util.Kangaroo
{
    /// <summary>This class implements the install window.</summary>
    internal partial class FormInstall: Form, IWindow
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Mouse hold position.</summary>
        private Point _MouseHold = new Point();
        


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormInstall()
        {
            InitializeComponent();

            _CboPath.Text = Program._ParsePath(Program._Install.Get("dir", @"<PROGRAM_FILES>\" + Program._Key));
            _LabelInstall.Text = _LabelInstall.Text.Replace("<ApplicationName>", Program._Install.Get("name", Program._Key));
            _LabelMessage.Text = "";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IWindow                                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Sets the window message.</summary>
        /// <param name="text">Message text.</param>
        /// <param name="progress">Progress.</param>
        public void SetProgress(string text, int progress)
        {
            if(text != null) { _LabelMessage.Text = text; }
            _PbarProgress.Value = progress;
            Application.DoEvents();
        }


        /// <summary>Sets the window message.</summary>
        /// <param name="text">Message text.</param>
        public void SetProgress(string text)
        {
            SetProgress(text, -1);
        }


        /// <summary>Sets the window message.</summary>
        /// <param name="progress">Progress.</param>
        public void SetProgress(int progress)
        {
            SetProgress(null, progress);
        }


        /// <summary>Signals the UI window to complete the installation process.</summary>
        /// <param name="success">Determines if the installation has succeeded.</param>
        public void Complete(bool success)
        {
            SetProgress(100);
            int n = _PbarProgress.Value;

            if(success)
            {
                _LabelInstall.Text = Program._Install.Get("name", Program._Key) + " has successfully been installed.";
                _LabelInstall.ForeColor = Color.Black;
            }
            else
            {
                _LabelInstall.Text = "The installation has failed.";
                _LabelInstall.ForeColor = Color.DarkRed;
            }

            _LabelMessage.Text = "";
            _ButtonCancel.Text = "&Close";
            _ButtonCancel.Visible = true;

            Height = 246;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Mouse down.</summary>
        private void _PanelInstall_MouseDown(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left) { _MouseHold = e.Location; }
        }


        /// <summary>Mouse move.</summary>
        private void _PanelInstall_MouseMove(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left) { Location = new Point(Location.X + (e.X - _MouseHold.X), Location.Y + (e.Y - _MouseHold.Y)); }
        }


        /// <summary>Button "Cancel" click.</summary>
        private void _ButtonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }


        /// <summary>Button "Browse" click.</summary>
        private void _ButtonBrowse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog d = new FolderBrowserDialog();
            d.SelectedPath = _CboPath.Text;
            if(d.ShowDialog() == DialogResult.OK)
            {
                _CboPath.Text = d.SelectedPath;
            }
        }


        /// <summary>Button "Start" click.</summary>
        private void _ButtonStart_Click(object sender, EventArgs e)
        {
            Height = 260;
            _LabelInstall.Text = Program._Install.Get("name", Program._Key) + " is being installed.";
            _LabelInstall.ForeColor = Color.DimGray;

            _LabelPath.Visible = _PanelPath.Visible = _ButtonBrowse.Visible =
                                 _ButtonStart.Visible = _ButtonCancel.Visible = false;
            _LabelMessage.Text = "Initializing setup...";
            Application.DoEvents();

            Program._InstallationPath = _CboPath.Text;
            Program._RunSetup();
        }


        /// <summary>Combo text changed.</summary>
        private void _CboPath_TextChanged(object sender, EventArgs e)
        {
            try
            {
                _ButtonStart.Enabled = (!string.IsNullOrWhiteSpace(Path.GetFullPath(_CboPath.Text)));
            }
            catch(Exception) { _ButtonStart.Enabled = false; }
        }
    }
}
