﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Runtime.Versioning;
using System.Security.Principal;
using System.Windows.Forms;

using Robbiblubber.Util.Win32;



namespace Robbiblubber.Util.Kangaroo
{
    /// <summary>This class contains the application startup and updater code.</summary>
    internal static class Program
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>User interface window.</summary>
        private static IWindow _Window;
        
        /// <summary>Key.</summary>
        internal static string _Key = null;

        /// <summary>Install ddp script from source.</summary>
        internal static Ddp _Install = null;

        /// <summary>Source path.</summary>
        internal static string _SourcePath = null;

        /// <summary>Local version signature file.</summary>
        internal static Ddp _LocalVsig = null;

        /// <summary>Installation path.</summary>
        internal static string _InstallationPath = null;

        /// <summary>Target application start command line..</summary>
        private static string _CommandLine = null;

        /// <summary>Number of files to copy.</summary>
        private static int _FileCount = 0;

        /// <summary>Progress.</summary>
        private static double _Progress = 10.0d;

        /// <summary>Step.</summary>
        private static double _Step;
        
        /// <summary>Admin flag.</summary>
        private static bool _IsAdmin;

        /// <summary>Uninstalling flag.</summary>
        private static bool _Uninstalling = false;

        /// <summary>Installing flag.</summary>
        private static bool _Installing = false;

        /// <summary>Verbose mode flag.</summary>
        private static bool _Verbose = false;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // entry point                                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>The main entry point for the application.</summary>
        /// <param name="args">Command line arguments.</param>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            WindowsPrincipal p = new WindowsPrincipal(WindowsIdentity.GetCurrent());
            _IsAdmin = p.IsInRole(WindowsBuiltInRole.Administrator);

                                                                                // get the key
            if(args.Length == 2)                                                // if arguments were passed, the key is an argument
            {
                _Uninstalling = ((args[0] == "-u") || (args[0] == "/u"));
                _Key = args[1];
            }
            else if(args.Length == 1)
            {
                _Key = args[0];
            }
            else                                                                // if no arguments were passed, an install.data file must be present
            {                                                                   // read source, installation path and startup command from instll.config
                
                if(!File.Exists(PathOp.ApplicationDirectory + @"\install.data"))
                {                                                               // older version might create update.config instead of install.data
                    if(File.Exists(PathOp.ApplicationDirectory + @"\update.config")) { File.Move(PathOp.ApplicationDirectory + @"\update.config", PathOp.ApplicationDirectory + @"\install.data"); }
                }
                
                Ddp ddp = Ddp.Open(PathOp.ApplicationDirectory + @"\install.data");

                _SourcePath = ddp.Get<string>("source");
                _InstallationPath = ddp.Get<string>("install");
                _CommandLine = ddp.Get<string>("start");
            }

            if(string.IsNullOrWhiteSpace(_Key))                                 // if no key is present, get the install.config from the source
            {                                                                   // and retrieve the key
                try
                {
                    if(_SourcePath.StartsWith("http://") || _SourcePath.StartsWith("https://") || _SourcePath.StartsWith("ftp://"))
                    {
                        _Install = Ddp.Open(_SourcePath.TrimEnd('/') + "/install.config");
                    }
                    else
                    {
                        _Install = Ddp.Open(_SourcePath.TrimEnd('\\') + @"\install.config");
                    }

                    _Key = _Install.Get<string>("key");
                }
                catch(Exception) {}
            }
            
            if(string.IsNullOrWhiteSpace(_Key))                                 // if still no key is present, something is fishy about the installation
            {
                MessageBox.Show("Invalid application key.", "Kangaroo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

                                                                                // get the local version signature
            _LocalVsig = Ddp.Open(PathOp.LocalConfigurationPath + @"\robbiblubber.org\instll\" + _Key + ".vsig");

                                                                                // if the source and installation paths haven't been read from install.data,
                                                                                // at least the source must be found in the local version signature
                                                                                // installation path may be empty if the package hasn't ever been installed yet
            if(string.IsNullOrWhiteSpace(_SourcePath)) { _SourcePath = _LocalVsig.Get<string>("source"); }
            if(string.IsNullOrWhiteSpace(_InstallationPath)) { _InstallationPath = _LocalVsig.Get<string>("install"); }

            if(string.IsNullOrWhiteSpace(_SourcePath))                          // in case we still have no source path, we're done
            {
                MessageBox.Show("Could not find installation source.", "Kangaroo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            _SourcePath = _SourcePath.TrimEnd('/', '\\', ' ');                  // clean up source path name

            
            if(_Uninstalling)
            {
                // TODO: uninstall!
            }
            else if(_Installing = string.IsNullOrWhiteSpace(_InstallationPath))
            {
                _Window = new FormInstall();
            }
            else
            {
                _Window = new FormUpdate();
            }

            Application.Run((Form) _Window);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Runs the application setup.</summary>
        /// <returns>Returns TRUE on success, otherwise returns FALSE.</returns>
        internal static bool _RunSetup()
        {
            _Window.SetProgress("Retrieving configuration...", 2);

            _Verbose = Ddp.Open(PathOp.LocalConfigurationPath + @"\robbiblubber.org\instll\instll.config").Get<bool>(_Key + "/verbose");
            _Log("\r\nStarted Installation [" + DateTime.Now.ToString("yyyy-mm-dd HH:MM:ss") + "]");
            _Log("Product: " + _Install.Get<string>("name"));
            _Log("Version: " + _Install.Get<string>("version"));

            _Window.SetProgress("Downloading package...", 6);

            try
            {                                                                   // download package
                string tempFile = PathOp.ApplicationDirectory + @"\install.bag";
                string temp = PathOp.ApplicationDirectory + @"\src";
                Directory.CreateDirectory(temp);

                if(_SourcePath.StartsWith("http://") || _SourcePath.StartsWith("https://") || _SourcePath.StartsWith("ftp://"))
                {
                    _LogVerbose("   Downloading file \"" + _SourcePath + "/install.bag\" to \"" + tempFile + "\"");
                    WebClient wc = new WebClient();
                    wc.DownloadFile(_SourcePath + "/install.bag", tempFile);
                }
                else
                {
                    _LogVerbose("   Copying file \"" + _SourcePath + "/install.bag\" to \"" + tempFile + "\"");
                    File.Copy(_SourcePath + @"\install.bag", tempFile);
                }
                _Window.SetProgress("Extracting package...", 10);

                _LogVerbose("   Extracting package to \"" + tempFile + "\".");
                ZipFile.ExtractToDirectory(tempFile, temp);                     // extract package
                _Window.SetProgress(8);
                
                _CountFiles(temp);                                              // count files
                _Step = Convert.ToDouble(80) / Convert.ToDouble(_FileCount);
                
                _Window.SetProgress("Copying files...", 20);

                if(!_Installing)
                {                                                               // delete files in "del" key
                    foreach(string i in _Install.Get<string[]>("del"))
                    {
                        try
                        {
                            FileOp.Delete(_InstallationPath + @"\" + i);
                        }
                        catch(Exception) { }
                    }
                }

                _CopyDir(temp, _InstallationPath);                              // copy files

                if(_Installing)
                {                                                               // create links
                    _Window.SetProgress("Creating links...", 95);

                    foreach(DdpElement i in _Install.Sections)
                    {
                        if(i.Get<string>("action") == "link")
                        {
                            string file = _ParsePath(i.Get<string>("file"));
                            string icon = i.Get<string>("icon", null);
                            if(icon != null) { icon = _ParsePath(icon); }
                            WFileOp.CreateLink(file, _ParsePath(i.Get<string>("target")), _ParsePath(i.Get<string>("args", null)), i.Get<string>("descr", Path.GetFileName(file)), icon, i.Get<int>("index"));
                        }
                    }
                }

                _Window.SetProgress("Registeringing installation...", 97);

                _LogVerbose("   Registeringing installation.");                 // register version signature
                if(!Directory.Exists(PathOp.LocalConfigurationPath + @"\robbiblubber.org\instll")) { Directory.CreateDirectory(PathOp.LocalConfigurationPath + @"\robbiblubber.org\instll"); }
                _LocalVsig.Set("key", _Key);
                _LocalVsig.Set("version", _Install.Get<string>("version"));
                _LocalVsig.Set("install", _InstallationPath);
                _LocalVsig.Set("source", _SourcePath);
                _LocalVsig.Save();

                foreach(DdpElement i in _Install.Sections)
                {                                                               // register application
                    if(i.Get<string>("action") == "register")
                    {
                        WEnvOp.RegisterApplication(_Key,
                                                  i.Get("name", _Install.Get<string>("name")),
                                                  i.Get("publisher", _Install.Get<string>("publisher")),
                                                  _InstallationPath,
                                                  i.Get<string>("icon", null),
                                                  new Version(_Install.Get<string>("version")),
                                                  _InstallationPath + @"\kangaroo.exe -u " + _Key);
                    }
                }

                _Window.SetProgress("Cleaning up...", 99);

                try { File.Delete(tempFile); } catch(Exception) {}              // remove temporary files
                try { Directory.Delete(temp, true); } catch(Exception) {}

                if(!string.IsNullOrWhiteSpace(_CommandLine))                    // start application if required
                {
                    _Window.SetProgress("Starting application...", 100);

                    try
                    {
                        _LogVerbose("   Starting \"" + _CommandLine + "\".");
                        if(_CommandLine.Contains(" "))
                        {
                            Process.Start(_CommandLine.Substring(0, _CommandLine.IndexOf(' ')), _CommandLine.Substring(_CommandLine.IndexOf(' ')).Trim());
                        }
                        else { Process.Start(_CommandLine.Trim()); }
                    }
                    catch(Exception ex) 
                    {
                        _LogVerbose("   " + ex.ToString().Replace("\r\n", "\r\n   ").TrimEnd());
                    }
                }
                _Window.SetProgress("Done.");
            }
            catch(Exception ex)
            {
                _LogVerbose("   " + ex.ToString().Replace("\r\n", "\r\n   ").TrimEnd());
                _Log("Installation failed [" + DateTime.Now.ToString("yyyy-mm-dd HH:MM:ss") + "]");
                _Window.Complete(false);
                return false; 
            }

            _Log("Installation completed [" + DateTime.Now.ToString("yyyy-mm-dd HH:MM:ss") + "]");
            _Window.Complete(true);
            return true;
        }


        /// <summary>Counts files in source directory.</summary>
        /// <param name="directory"></param>
        private static void _CountFiles(string directory)
        {
            _FileCount += Directory.GetFiles(directory).Length;
            foreach(string i in Directory.GetDirectories(directory)) _CountFiles(i);
        }


        /// <summary>Recursively copies a directory.</summary>
        /// <param name="src">Source directory.</param>
        /// <param name="dest">Destination directory.</param>
        private static void _CopyDir(string src, string dest)
        {
            if(!Directory.Exists(dest)) { Directory.CreateDirectory(dest); }

            foreach(string i in Directory.GetFiles(src))
            {
                File.Copy(i, dest + @"\" + Path.GetFileName(i), true);

                _Progress += _Step;
                _Window.SetProgress(Convert.ToInt32(_Progress));
            }

            foreach(string i in Directory.GetDirectories(src))
            {
                _CopyDir(i, dest + @"\" + Path.GetFileName(i));
            }
        }


        /// <summary>Gets files from a list (including wildcards).</summary>
        /// <param name="data">File list.</param>
        /// <returns>File names.</returns>
        private static List<string> _GetFiles(string[] data)
        {
            List<string> rval = new List<string>();
            string[] l;

            foreach(string i in data)
            {
                if(i.StartsWith(@"*\"))
                {
                    l = Directory.GetFiles(_InstallationPath, Path.GetFileName(i), SearchOption.AllDirectories);
                }
                else if(i.Contains(@"\"))
                {
                    l = Directory.GetFiles(_InstallationPath + @"\" + Path.GetDirectoryName(i), Path.GetFileName(i));
                }
                else
                {
                    l = Directory.GetFiles(_InstallationPath, i);
                }

                foreach(string j in l) { rval.Add(j); }
            }

            return rval;
        }


        /// <summary>Parse a path.</summary>
        /// <param name="path">Path.</param>
        /// <returns>Parsed path.</returns>
        internal static string _ParsePath(string path)
        {
            return path.Replace("<PROGRAM_FILES>", Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles))
                       .Replace("<DESKTOP>", _IsAdmin ? Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory) : Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory))
                       .Replace("<USER_DESKTOP>", Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory))
                       .Replace("<COMMON_DESKTOP>", Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory))
                       .Replace("<PROGRAMS>", _IsAdmin ? Environment.GetFolderPath(Environment.SpecialFolder.CommonPrograms) : Environment.GetFolderPath(Environment.SpecialFolder.Programs))
                       .Replace("<USER_PROGRAMS>", Environment.GetFolderPath(Environment.SpecialFolder.Programs))
                       .Replace("<COMMON_PROGRAMS>", Environment.GetFolderPath(Environment.SpecialFolder.CommonPrograms))
                       .Replace("<APPLICATION_DATA>", _IsAdmin ? Environment.GetFolderPath(Environment.SpecialFolder.CommonPrograms) : Environment.GetFolderPath(Environment.SpecialFolder.Programs))
                       .Replace("<USER_APPLICATION_DATA>", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData))
                       .Replace("<COMMON_APPLICATION_DATA>", Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData))
                       .Replace("<PROGRAM_DATA>", Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData))
                       .Replace("<FONTS>", Environment.GetFolderPath(Environment.SpecialFolder.Fonts))
                       .Replace("<START>", Environment.GetFolderPath(Environment.SpecialFolder.StartMenu))
                       .Replace("<START_MENU>", Environment.GetFolderPath(Environment.SpecialFolder.StartMenu))
                       .Replace("<STARTUP>", Environment.GetFolderPath(Environment.SpecialFolder.Startup))
                       .Replace("<SYSTEM>", Environment.GetFolderPath(Environment.SpecialFolder.System))
                       .Replace("<SYSTEM_X86>", Environment.GetFolderPath(Environment.SpecialFolder.SystemX86))
                       .Replace("<WINDOWS>", Environment.GetFolderPath(Environment.SpecialFolder.Windows))
                       .Replace("<USER_PROFILE>", Environment.GetFolderPath(Environment.SpecialFolder.UserProfile));
        }


        /// <summary>Writes a log entry.</summary>
        /// <param name="text">Log text.</param>
        internal static void _Log(string text = "")
        {
            File.AppendAllText(PathOp.LocalConfigurationPath + @"\robbiblubber.org\instll\" + _Key + ".log", text + "\r\n");
        }


        /// <summary>Writes a verbose mode log entry.</summary>
        /// <param name="text">Log text.</param>
        internal static void _LogVerbose(string text = "")
        {
            if(_Verbose) _Log(text);
        }
    }
}
