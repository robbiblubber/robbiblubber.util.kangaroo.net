﻿namespace Robbiblubber.Util.Kangaroo
{
    partial class FormInstall
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormInstall));
            this._PanelInstall = new System.Windows.Forms.Panel();
            this._PbarProgress = new Robbiblubber.Util.Controls.ProgressControl();
            this._LabelMessage = new System.Windows.Forms.Label();
            this._LabelKangaroo = new System.Windows.Forms.Label();
            this._LabelRobbiblubber = new System.Windows.Forms.Label();
            this._LabelPath = new System.Windows.Forms.Label();
            this._LabelInstall = new System.Windows.Forms.Label();
            this._ButtonCancel = new System.Windows.Forms.Button();
            this._ButtonStart = new System.Windows.Forms.Button();
            this._ButtonBrowse = new System.Windows.Forms.Button();
            this._PanelPath = new System.Windows.Forms.Panel();
            this._CboPath = new System.Windows.Forms.ComboBox();
            this._PanelInstall.SuspendLayout();
            this._PanelPath.SuspendLayout();
            this.SuspendLayout();
            // 
            // _PanelInstall
            // 
            this._PanelInstall.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._PanelInstall.Controls.Add(this._PbarProgress);
            this._PanelInstall.Controls.Add(this._LabelMessage);
            this._PanelInstall.Controls.Add(this._LabelKangaroo);
            this._PanelInstall.Controls.Add(this._LabelRobbiblubber);
            this._PanelInstall.Controls.Add(this._LabelPath);
            this._PanelInstall.Controls.Add(this._LabelInstall);
            this._PanelInstall.Controls.Add(this._ButtonCancel);
            this._PanelInstall.Controls.Add(this._ButtonStart);
            this._PanelInstall.Controls.Add(this._ButtonBrowse);
            this._PanelInstall.Controls.Add(this._PanelPath);
            this._PanelInstall.Location = new System.Drawing.Point(0, 0);
            this._PanelInstall.Name = "_PanelInstall";
            this._PanelInstall.Size = new System.Drawing.Size(560, 260);
            this._PanelInstall.TabIndex = 0;
            this._PanelInstall.MouseDown += new System.Windows.Forms.MouseEventHandler(this._PanelInstall_MouseDown);
            this._PanelInstall.MouseMove += new System.Windows.Forms.MouseEventHandler(this._PanelInstall_MouseMove);
            // 
            // _PbarProgress
            // 
            this._PbarProgress.BarColor = System.Drawing.Color.CornflowerBlue;
            this._PbarProgress.ForeColor = System.Drawing.Color.DarkGray;
            this._PbarProgress.Location = new System.Drawing.Point(-1, 244);
            this._PbarProgress.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._PbarProgress.Maximum = 100;
            this._PbarProgress.Name = "_PbarProgress";
            this._PbarProgress.Size = new System.Drawing.Size(560, 15);
            this._PbarProgress.TabIndex = 35;
            this._PbarProgress.Value = 0;
            // 
            // _LabelMessage
            // 
            this._LabelMessage.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelMessage.ForeColor = System.Drawing.Color.Gray;
            this._LabelMessage.Location = new System.Drawing.Point(110, 227);
            this._LabelMessage.Name = "_LabelMessage";
            this._LabelMessage.Size = new System.Drawing.Size(446, 13);
            this._LabelMessage.TabIndex = 28;
            this._LabelMessage.Text = "Kangaroo installer is checking installations...";
            this._LabelMessage.MouseDown += new System.Windows.Forms.MouseEventHandler(this._PanelInstall_MouseDown);
            this._LabelMessage.MouseMove += new System.Windows.Forms.MouseEventHandler(this._PanelInstall_MouseMove);
            // 
            // _LabelKangaroo
            // 
            this._LabelKangaroo.Image = ((System.Drawing.Image)(resources.GetObject("_LabelKangaroo.Image")));
            this._LabelKangaroo.Location = new System.Drawing.Point(7, 173);
            this._LabelKangaroo.Name = "_LabelKangaroo";
            this._LabelKangaroo.Size = new System.Drawing.Size(78, 79);
            this._LabelKangaroo.TabIndex = 26;
            this._LabelKangaroo.MouseDown += new System.Windows.Forms.MouseEventHandler(this._PanelInstall_MouseDown);
            this._LabelKangaroo.MouseMove += new System.Windows.Forms.MouseEventHandler(this._PanelInstall_MouseMove);
            // 
            // _LabelRobbiblubber
            // 
            this._LabelRobbiblubber.Image = ((System.Drawing.Image)(resources.GetObject("_LabelRobbiblubber.Image")));
            this._LabelRobbiblubber.Location = new System.Drawing.Point(439, 13);
            this._LabelRobbiblubber.Name = "_LabelRobbiblubber";
            this._LabelRobbiblubber.Size = new System.Drawing.Size(117, 29);
            this._LabelRobbiblubber.TabIndex = 25;
            this._LabelRobbiblubber.MouseDown += new System.Windows.Forms.MouseEventHandler(this._PanelInstall_MouseDown);
            this._LabelRobbiblubber.MouseMove += new System.Windows.Forms.MouseEventHandler(this._PanelInstall_MouseMove);
            // 
            // _LabelPath
            // 
            this._LabelPath.AutoSize = true;
            this._LabelPath.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelPath.ForeColor = System.Drawing.Color.DimGray;
            this._LabelPath.Location = new System.Drawing.Point(43, 85);
            this._LabelPath.Name = "_LabelPath";
            this._LabelPath.Size = new System.Drawing.Size(94, 13);
            this._LabelPath.TabIndex = 34;
            this._LabelPath.Text = "Installation &Path:";
            this._LabelPath.MouseDown += new System.Windows.Forms.MouseEventHandler(this._PanelInstall_MouseDown);
            this._LabelPath.MouseMove += new System.Windows.Forms.MouseEventHandler(this._PanelInstall_MouseMove);
            // 
            // _LabelInstall
            // 
            this._LabelInstall.Location = new System.Drawing.Point(42, 49);
            this._LabelInstall.Name = "_LabelInstall";
            this._LabelInstall.Size = new System.Drawing.Size(495, 48);
            this._LabelInstall.TabIndex = 33;
            this._LabelInstall.Text = "A Kangaroo is about to install <ApplicationName> on your computer.";
            this._LabelInstall.MouseDown += new System.Windows.Forms.MouseEventHandler(this._PanelInstall_MouseDown);
            this._LabelInstall.MouseMove += new System.Windows.Forms.MouseEventHandler(this._PanelInstall_MouseMove);
            // 
            // _ButtonCancel
            // 
            this._ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonCancel.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this._ButtonCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this._ButtonCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this._ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCancel.Location = new System.Drawing.Point(363, 153);
            this._ButtonCancel.Name = "_ButtonCancel";
            this._ButtonCancel.Size = new System.Drawing.Size(137, 30);
            this._ButtonCancel.TabIndex = 32;
            this._ButtonCancel.Text = "&Cancel";
            this._ButtonCancel.UseVisualStyleBackColor = true;
            this._ButtonCancel.Click += new System.EventHandler(this._ButtonCancel_Click);
            // 
            // _ButtonStart
            // 
            this._ButtonStart.Enabled = false;
            this._ButtonStart.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this._ButtonStart.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this._ButtonStart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this._ButtonStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonStart.Location = new System.Drawing.Point(220, 153);
            this._ButtonStart.Name = "_ButtonStart";
            this._ButtonStart.Size = new System.Drawing.Size(137, 30);
            this._ButtonStart.TabIndex = 31;
            this._ButtonStart.Text = "&Start";
            this._ButtonStart.UseVisualStyleBackColor = true;
            this._ButtonStart.Click += new System.EventHandler(this._ButtonStart_Click);
            // 
            // _ButtonBrowse
            // 
            this._ButtonBrowse.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this._ButtonBrowse.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this._ButtonBrowse.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this._ButtonBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonBrowse.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonBrowse.Image")));
            this._ButtonBrowse.Location = new System.Drawing.Point(475, 101);
            this._ButtonBrowse.Name = "_ButtonBrowse";
            this._ButtonBrowse.Size = new System.Drawing.Size(25, 25);
            this._ButtonBrowse.TabIndex = 30;
            this._ButtonBrowse.UseVisualStyleBackColor = true;
            this._ButtonBrowse.Click += new System.EventHandler(this._ButtonBrowse_Click);
            // 
            // _PanelPath
            // 
            this._PanelPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._PanelPath.Controls.Add(this._CboPath);
            this._PanelPath.Location = new System.Drawing.Point(45, 100);
            this._PanelPath.Name = "_PanelPath";
            this._PanelPath.Size = new System.Drawing.Size(424, 25);
            this._PanelPath.TabIndex = 29;
            // 
            // _CboPath
            // 
            this._CboPath.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this._CboPath.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.FileSystem;
            this._CboPath.BackColor = System.Drawing.Color.White;
            this._CboPath.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._CboPath.ForeColor = System.Drawing.Color.Black;
            this._CboPath.FormattingEnabled = true;
            this._CboPath.Location = new System.Drawing.Point(0, 0);
            this._CboPath.Name = "_CboPath";
            this._CboPath.Size = new System.Drawing.Size(423, 25);
            this._CboPath.TabIndex = 8;
            this._CboPath.TextChanged += new System.EventHandler(this._CboPath_TextChanged);
            // 
            // FormInstall
            // 
            this.AcceptButton = this._ButtonStart;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CancelButton = this._ButtonCancel;
            this.ClientSize = new System.Drawing.Size(560, 246);
            this.Controls.Add(this._PanelInstall);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "FormInstall";
            this.Text = "Kangaroo Insteller";
            this._PanelInstall.ResumeLayout(false);
            this._PanelInstall.PerformLayout();
            this._PanelPath.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel _PanelInstall;
        internal System.Windows.Forms.Label _LabelMessage;
        private System.Windows.Forms.Label _LabelKangaroo;
        private System.Windows.Forms.Label _LabelRobbiblubber;
        private System.Windows.Forms.Label _LabelPath;
        private System.Windows.Forms.Label _LabelInstall;
        private System.Windows.Forms.Button _ButtonCancel;
        private System.Windows.Forms.Button _ButtonStart;
        private System.Windows.Forms.Button _ButtonBrowse;
        private System.Windows.Forms.Panel _PanelPath;
        private System.Windows.Forms.ComboBox _CboPath;
        private Util.Controls.ProgressControl _PbarProgress;
    }
}