﻿using System;
using System.IO;
using System.Windows.Forms;

using Robbiblubber.Util.Prelude;



namespace Robbiblubber.Util.Kangaroo.Builder
{
    /// <summary>This class contains the application startup code.</summary>
    static class Program
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // entry point                                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>The main entry point for the application.</summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormBuild());
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal static methods                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Builds a setup file.</summary>
        /// <param name="fileName">File name.</param>
        /// <param name="src">Source URL.</param>
        /// <param name="subfolder">Folder containing the correct launcher file.</param>
        internal static void _Build(string fileName, string src, string subfolder)
        {
            string data = PathOp.TempRoot + @"\" + StringOp.Unique() + ".data";

            Ddp ddp = new Ddp();
            ddp.Set("source", src);
            ddp.Save(data);

            File.Copy(PathOp.ApplicationDirectory + @"\" + subfolder + @"\launch.exe", fileName);

            PreludedData.AppendToFile(data, fileName);

            try
            {
                File.Delete(data);
            }
            catch(Exception) {}
        }
    }
}
