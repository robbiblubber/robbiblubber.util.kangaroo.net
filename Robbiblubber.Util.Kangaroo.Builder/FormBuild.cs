﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;



namespace Robbiblubber.Util.Kangaroo.Builder
{
    /// <summary>This class implements the builder window.</summary>
    public partial class FormBuild: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Mouse hold position.</summary>
        private Point _MouseHold = new Point();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormBuild()
        {
            InitializeComponent();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Button "Close" click.</summary>
        private void _ButtonClose_Click(object sender, EventArgs e)
        {
            Close();
        }


        /// <summary>Mouse down.</summary>
        private void _PanelBuild_MouseDown(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left) { _MouseHold = e.Location; }
        }


        /// <summary>Mouse move.</summary>
        private void _PanelBuild_MouseMove(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left) { Location = new Point(Location.X + (e.X - _MouseHold.X), Location.Y + (e.Y - _MouseHold.Y)); }
        }


        /// <summary>Button "Build" click.</summary>
        private void _ButtonBuild_Click(object sender, EventArgs e)
        {
            SaveFileDialog d = new SaveFileDialog();
            d.Filter = "Executables (*.exe)|*.exe|All Files|*.*";
            d.FileName = "instll.exe";

            if(d.ShowDialog() == DialogResult.OK)
            {
                string folder = "5.0";
                if(!_CheckNET5.Checked)
                {
                    folder = "4.8";
                }
                else if(!_CheckSelfContain.Checked) { folder = "5.0h"; }

                if(File.Exists(d.FileName))
                {
                    File.Delete(d.FileName);
                }
                Program._Build(d.FileName, _TextSource.Text, folder);
            }
        }


        /// <summary>Check ".NET 5" changed.</summary>
        private void _CheckNET5_CheckedChanged(object sender, EventArgs e)
        {
            if(!_CheckNET5.Checked) { _CheckSelfContain.Checked = false; }
        }


        /// <summary>Check "Self Contained" changed.</summary>
        private void _CheckSelfContain_CheckedChanged(object sender, EventArgs e)
        {
            if(_CheckSelfContain.Checked) { _CheckNET5.Checked = true; }
        }
    }
}
