﻿namespace Robbiblubber.Util.Kangaroo.Builder
{
    partial class FormBuild
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBuild));
            this._PanelBuild = new System.Windows.Forms.Panel();
            this._CheckNET5 = new System.Windows.Forms.CheckBox();
            this._LabelSource = new System.Windows.Forms.Label();
            this._TextSource = new System.Windows.Forms.TextBox();
            this._ButtonClose = new System.Windows.Forms.Button();
            this._ButtonBuild = new System.Windows.Forms.Button();
            this._PanelTitle = new System.Windows.Forms.Panel();
            this._LabelTitle = new System.Windows.Forms.Label();
            this._LabelRobbiblubber = new System.Windows.Forms.Label();
            this._LabelSymbol = new System.Windows.Forms.Label();
            this._CheckSelfContain = new System.Windows.Forms.CheckBox();
            this._PanelBuild.SuspendLayout();
            this._PanelTitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // _PanelBuild
            // 
            this._PanelBuild.BackColor = System.Drawing.Color.WhiteSmoke;
            this._PanelBuild.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._PanelBuild.Controls.Add(this._CheckSelfContain);
            this._PanelBuild.Controls.Add(this._CheckNET5);
            this._PanelBuild.Controls.Add(this._LabelSource);
            this._PanelBuild.Controls.Add(this._TextSource);
            this._PanelBuild.Controls.Add(this._ButtonClose);
            this._PanelBuild.Controls.Add(this._ButtonBuild);
            this._PanelBuild.Controls.Add(this._PanelTitle);
            this._PanelBuild.Location = new System.Drawing.Point(0, 0);
            this._PanelBuild.Name = "_PanelBuild";
            this._PanelBuild.Size = new System.Drawing.Size(640, 240);
            this._PanelBuild.TabIndex = 0;
            this._PanelBuild.MouseDown += new System.Windows.Forms.MouseEventHandler(this._PanelBuild_MouseDown);
            this._PanelBuild.MouseMove += new System.Windows.Forms.MouseEventHandler(this._PanelBuild_MouseMove);
            // 
            // _CheckNET5
            // 
            this._CheckNET5.AutoSize = true;
            this._CheckNET5.Checked = true;
            this._CheckNET5.CheckState = System.Windows.Forms.CheckState.Checked;
            this._CheckNET5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._CheckNET5.Location = new System.Drawing.Point(32, 143);
            this._CheckNET5.Name = "_CheckNET5";
            this._CheckNET5.Size = new System.Drawing.Size(157, 17);
            this._CheckNET5.TabIndex = 3;
            this._CheckNET5.Text = " Create Setup for .NET 5.0";
            this._CheckNET5.UseVisualStyleBackColor = true;
            this._CheckNET5.CheckedChanged += new System.EventHandler(this._CheckNET5_CheckedChanged);
            // 
            // _LabelSource
            // 
            this._LabelSource.AutoSize = true;
            this._LabelSource.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._LabelSource.ForeColor = System.Drawing.Color.Gray;
            this._LabelSource.Location = new System.Drawing.Point(29, 91);
            this._LabelSource.Name = "_LabelSource";
            this._LabelSource.Size = new System.Drawing.Size(129, 13);
            this._LabelSource.TabIndex = 0;
            this._LabelSource.Text = "Installation Source &URL:";
            this._LabelSource.MouseDown += new System.Windows.Forms.MouseEventHandler(this._PanelBuild_MouseDown);
            this._LabelSource.MouseMove += new System.Windows.Forms.MouseEventHandler(this._PanelBuild_MouseMove);
            // 
            // _TextSource
            // 
            this._TextSource.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextSource.Location = new System.Drawing.Point(32, 107);
            this._TextSource.Name = "_TextSource";
            this._TextSource.Size = new System.Drawing.Size(569, 25);
            this._TextSource.TabIndex = 0;
            this._TextSource.Text = "http://";
            // 
            // _ButtonClose
            // 
            this._ButtonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonClose.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this._ButtonClose.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this._ButtonClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this._ButtonClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonClose.Location = new System.Drawing.Point(464, 173);
            this._ButtonClose.Name = "_ButtonClose";
            this._ButtonClose.Size = new System.Drawing.Size(137, 30);
            this._ButtonClose.TabIndex = 2;
            this._ButtonClose.Text = "&Close";
            this._ButtonClose.UseVisualStyleBackColor = true;
            this._ButtonClose.Click += new System.EventHandler(this._ButtonClose_Click);
            // 
            // _ButtonBuild
            // 
            this._ButtonBuild.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this._ButtonBuild.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this._ButtonBuild.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this._ButtonBuild.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonBuild.Location = new System.Drawing.Point(321, 173);
            this._ButtonBuild.Name = "_ButtonBuild";
            this._ButtonBuild.Size = new System.Drawing.Size(137, 30);
            this._ButtonBuild.TabIndex = 1;
            this._ButtonBuild.Text = "&Build";
            this._ButtonBuild.UseVisualStyleBackColor = true;
            this._ButtonBuild.Click += new System.EventHandler(this._ButtonBuild_Click);
            // 
            // _PanelTitle
            // 
            this._PanelTitle.BackColor = System.Drawing.SystemColors.Window;
            this._PanelTitle.Controls.Add(this._LabelTitle);
            this._PanelTitle.Controls.Add(this._LabelRobbiblubber);
            this._PanelTitle.Controls.Add(this._LabelSymbol);
            this._PanelTitle.Location = new System.Drawing.Point(0, 0);
            this._PanelTitle.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._PanelTitle.Name = "_PanelTitle";
            this._PanelTitle.Size = new System.Drawing.Size(639, 60);
            this._PanelTitle.TabIndex = 0;
            this._PanelTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this._PanelBuild_MouseDown);
            this._PanelTitle.MouseMove += new System.Windows.Forms.MouseEventHandler(this._PanelBuild_MouseMove);
            // 
            // _LabelTitle
            // 
            this._LabelTitle.AutoSize = true;
            this._LabelTitle.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this._LabelTitle.Location = new System.Drawing.Point(38, 27);
            this._LabelTitle.Name = "_LabelTitle";
            this._LabelTitle.Size = new System.Drawing.Size(214, 25);
            this._LabelTitle.TabIndex = 0;
            this._LabelTitle.Text = "Kangaroo Setup Builder";
            this._LabelTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this._PanelBuild_MouseDown);
            this._LabelTitle.MouseMove += new System.Windows.Forms.MouseEventHandler(this._PanelBuild_MouseMove);
            // 
            // _LabelRobbiblubber
            // 
            this._LabelRobbiblubber.Image = ((System.Drawing.Image)(resources.GetObject("_LabelRobbiblubber.Image")));
            this._LabelRobbiblubber.Location = new System.Drawing.Point(-3, 0);
            this._LabelRobbiblubber.Name = "_LabelRobbiblubber";
            this._LabelRobbiblubber.Size = new System.Drawing.Size(123, 29);
            this._LabelRobbiblubber.TabIndex = 0;
            this._LabelRobbiblubber.MouseDown += new System.Windows.Forms.MouseEventHandler(this._PanelBuild_MouseDown);
            this._LabelRobbiblubber.MouseMove += new System.Windows.Forms.MouseEventHandler(this._PanelBuild_MouseMove);
            // 
            // _LabelSymbol
            // 
            this._LabelSymbol.Image = ((System.Drawing.Image)(resources.GetObject("_LabelSymbol.Image")));
            this._LabelSymbol.Location = new System.Drawing.Point(570, 1);
            this._LabelSymbol.Name = "_LabelSymbol";
            this._LabelSymbol.Size = new System.Drawing.Size(65, 59);
            this._LabelSymbol.TabIndex = 0;
            this._LabelSymbol.MouseDown += new System.Windows.Forms.MouseEventHandler(this._PanelBuild_MouseDown);
            this._LabelSymbol.MouseMove += new System.Windows.Forms.MouseEventHandler(this._PanelBuild_MouseMove);
            // 
            // _CheckSelfContain
            // 
            this._CheckSelfContain.AutoSize = true;
            this._CheckSelfContain.Checked = true;
            this._CheckSelfContain.CheckState = System.Windows.Forms.CheckState.Checked;
            this._CheckSelfContain.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._CheckSelfContain.Location = new System.Drawing.Point(32, 163);
            this._CheckSelfContain.Name = "_CheckSelfContain";
            this._CheckSelfContain.Size = new System.Drawing.Size(143, 17);
            this._CheckSelfContain.TabIndex = 4;
            this._CheckSelfContain.Text = " Create self-containing";
            this._CheckSelfContain.UseVisualStyleBackColor = true;
            this._CheckSelfContain.CheckedChanged += new System.EventHandler(this._CheckSelfContain_CheckedChanged);
            // 
            // FormBuild
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonClose;
            this.ClientSize = new System.Drawing.Size(640, 240);
            this.Controls.Add(this._PanelBuild);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "FormBuild";
            this.Text = "Kangaroo Setup Builder";
            this._PanelBuild.ResumeLayout(false);
            this._PanelBuild.PerformLayout();
            this._PanelTitle.ResumeLayout(false);
            this._PanelTitle.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel _PanelBuild;
        private System.Windows.Forms.Panel _PanelTitle;
        private System.Windows.Forms.Label _LabelRobbiblubber;
        private System.Windows.Forms.Label _LabelSymbol;
        private System.Windows.Forms.Label _LabelTitle;
        private System.Windows.Forms.Label _LabelSource;
        private System.Windows.Forms.TextBox _TextSource;
        private System.Windows.Forms.Button _ButtonClose;
        private System.Windows.Forms.Button _ButtonBuild;
        private System.Windows.Forms.CheckBox _CheckNET5;
        private System.Windows.Forms.CheckBox _CheckSelfContain;
    }
}

