﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Reflection;
using System.Windows.Forms;
using Robbiblubber.Util.Prelude;



namespace Robbiblubber.Util.Kangaroo.Launcher
{
    /// <summary>This class contains the application startup code.</summary>
    static class Program
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // entry point                                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>The main entry point for the application.</summary>
        [STAThread]
        static void Main()
        {
            string tempPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\robbiblubber.org\temp";
            if(!Directory.Exists(tempPath)) { Directory.CreateDirectory(tempPath); }
            int n = 676200;

            while(Directory.Exists(tempPath))
            {
                tempPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\robbiblubber.org\temp\" + Convert.ToString(n, 16);
                n++;
            }
            Directory.CreateDirectory(tempPath);

            File.Copy(Environment.GetCommandLineArgs()[0], tempPath + @"\launch.crate");
            
            PreludedData.ExtractFromFile(tempPath + @"\launch.crate", tempPath + @"\install.data");
            
            WebClient wc = new WebClient();
            wc.DownloadFile("http://robbiblubber.org/instll/kangaroo/5.0/kangaroo.bag", tempPath + @"\kangaroo.bag");
            wc.Dispose();

            ZipFile.ExtractToDirectory(tempPath + @"\kangaroo.bag", tempPath);
            
            Process.Start(tempPath + @"\kangaroo.exe");
        }
    }
}
